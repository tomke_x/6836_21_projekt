import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.adresstrony)

WebUI.setText(findTestObject('Object Repository/Page_Credit Agricole Bank Polska S.A/input_Twj identyfikator_login'), findTestData(
        'TestLink').getValue(1, 1))

WebUI.click(findTestObject('Object Repository/Page_Credit Agricole Bank Polska S.A/button_DALEJ'))

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Credit Agricole Bank Polska S.A/input_Identyfikator abcdef1 Obrazek bezpiec_621845'), 
    findTestData('TestLink').getValue(2, 1))

WebUI.click(findTestObject('Object Repository/Page_Credit Agricole Bank Polska S.A/button_ZATWIERD'))

WebUI.click(findTestObject('Object Repository/Page_Credit Agricole Bank Polska S.A/span_Przelewy'))

WebUI.click(findTestObject('Object Repository/Page_Credit Agricole Bank Polska S.A/span_Odbiorcy'))

WebUI.click(findTestObject('Object Repository/Page_Credit Agricole Bank Polska S.A/a_Nowy odbiorca'))

WebUI.setText(findTestObject('Object Repository/Page_Credit Agricole Bank Polska S.A/input_Odbiorcy_beneficiaryCustomName'), 
    'testowy')

WebUI.setText(findTestObject('Object Repository/Page_Credit Agricole Bank Polska S.A/input_Odbiorcy_beneficiaryName'), findTestData(
        'TestLink').getValue(3, 1))

WebUI.setText(findTestObject('Object Repository/Page_Credit Agricole Bank Polska S.A/input_Odbiorcy_recipient-iban'), findTestData(
        'TestLink').getValue(4, 1))

WebUI.click(findTestObject('Object Repository/Page_Credit Agricole Bank Polska S.A/button_POTWIERD'))

WebUI.setText(findTestObject('Object Repository/Page_Credit Agricole Bank Polska S.A/textarea_Tytuem_title'), findTestData(
        'TestLink').getValue(5, 1))

WebUI.click(findTestObject('Object Repository/Page_Credit Agricole Bank Polska S.A/button_DALEJ_1'))

WebUI.click(findTestObject('Page_Credit Agricole Bank Polska S.A/ZATWIERDZ_2'))

WebUI.closeBrowser()

